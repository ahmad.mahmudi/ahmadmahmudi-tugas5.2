import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
} from 'react-native';
import React, {useState} from 'react';
import {useSelector} from 'react-redux';
import {useEffect} from 'react';

const Home = ({navigation, route}) => {
  const {product} = useSelector(state => state.data);
  const {isLoggedIn, userData} = useSelector(state => state.auth);
  console.log('cek userData', userData);
  console.log('cek product', product);
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <View style={{width: '15%'}} />
        <View
          style={{
            width: '70%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{fontSize: 16, fontWeight: '600', color: '#fff'}}>
            Home
          </Text>
        </View>
        <View style={{width: '15%'}} />
      </View>
      <FlatList
        data={product}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <TouchableOpacity
            style={styles.carditem}
            onPress={() => navigation.navigate('addData', {item})}>
            <View style={[styles.viewInfo, {marginTop: 0}]}>
              <View style={{width: '30%'}}>
                <Text style={{fontSize: 16, fontWeight: '600', color: '#000'}}>
                  ID
                </Text>
              </View>
              <View>
                <Text style={{color: '#000'}}>:</Text>
              </View>
              <View>
                <Text style={{color: '#000', fontWeight: 'bold'}}>
                  {item.id}
                </Text>
              </View>
            </View>
            <View style={styles.viewInfo}>
              <View style={{width: '30%'}}>
                <Text style={{fontSize: 16, fontWeight: '600', color: '#000'}}>
                  Nama Lengkap
                </Text>
              </View>
              <View
                style={{
                  width: '5%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{color: '#000'}}>:</Text>
              </View>
              <View>
                <Text style={{color: '#000', fontWeight: 'bold'}}>
                  {item.fullName}
                </Text>
              </View>
            </View>
            <View style={styles.viewInfo}>
              <View style={{width: '30%'}}>
                <Text style={{fontSize: 16, fontWeight: '600', color: '#000'}}>
                  Email
                </Text>
              </View>
              <View
                style={{
                  width: '5%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{color: '#000'}}>:</Text>
              </View>
              <View>
                <Text style={{color: '#000', fontWeight: 'bold'}}>
                  {item.email}
                </Text>
              </View>
            </View>
            <View style={styles.viewInfo}>
              <View style={{width: '30%'}}>
                <Text style={{fontSize: 16, fontWeight: '600', color: '#000'}}>
                  Alamat
                </Text>
              </View>
              <View
                style={{
                  width: '5%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{color: '#000'}}>:</Text>
              </View>
              <View>
                <Text style={{color: '#000', fontWeight: 'bold'}}>
                  {item.address}
                </Text>
              </View>
            </View>
            <View style={styles.viewInfo}>
              <View style={{width: '30%'}}>
                <Text style={{fontSize: 16, fontWeight: '600', color: '#000'}}>
                  Telepon
                </Text>
              </View>
              <View
                style={{
                  width: '5%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{color: '#000'}}>:</Text>
              </View>
              <View>
                <Text style={{color: '#000', fontWeight: 'bold'}}>
                  {item.phone}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        )}
      />
      <TouchableOpacity
        style={styles.btn}
        onPress={() => navigation.navigate('addData')}>
        <Image
          style={{width: 30, height: 30}}
          source={require('../../assets/Picture/icon/plus_black.png')}
        />
      </TouchableOpacity>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  carditem: {
    width: '90%',
    alignSelf: 'center',
    marginTop: 15,
    borderRadius: 4,
    borderColor: '#dedede',
    boederWidth: 1,
    padding: 15,
  },
  viewInfo: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },
  header: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#3f51b5',
    paddingVertical: 15,
  },
  text: {
    fontSize: 16,
    marginTop: 20,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  btn: {
    position: 'absolute',
    bottom: 30,
    right: 30,
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: 'aqua',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
