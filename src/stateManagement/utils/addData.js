import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native';
import React, {useEffect} from 'react';
import {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';

const AddData = ({navigation, route}) => {
  const [nama, setNama] = useState('');
  const [email, setEmail] = useState('');
  const [alamat, setAlamat] = useState('');
  const [telepon, setTelepon] = useState('');
  const dispatch = useDispatch();
  const {product} = useSelector(state => state.data);

  const checkData = () => {
    if (route.params) {
      const data = route.params.item;
      setNama(data.fullName);
      setEmail(data.email);
      setAlamat(data.address);
      setTelepon(data.phone);
    }
  };

  useEffect(() => {
    checkData();
  }, []);

  const updateData = () => {
    const data = {
      id: route.params.item.id,
      fullName: nama,
      email: email,
      address: alamat,
      phone: telepon,
    };
    dispatch({type: 'UPDATE_DATA', data});
    navigation.goBack();
  };

  const storeData = () => {
    var date = new Date();
    var dataProduct = [...product];
    const data = {
      id: date.getMilliseconds(),
      fullName: nama,
      email: email,
      address: alamat,
      phone: telepon,
    };
    dataProduct.push(data);
    dispatch({type: 'ADD_DATA', data: dataProduct});
    navigation.goBack();
  };

  const deleteData = () => {
    dispatch({type: 'DELETE_DATA', id: route.params.item.id});
    navigation.goBack();
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity
          style={styles.backbtn}
          onPress={() => navigation.goBack()}>
          <Image
            style={{width: 30, height: 30}}
            source={require('../../assets/Picture/icon/arrow_left_black.png')}
          />
        </TouchableOpacity>
        <View style={styles.titlebox}>
          <Text style={styles.titletxt}>
            {route.params ? 'Ubah Data' : 'Tambah Data'}
          </Text>
        </View>
        <View />
      </View>
      <View style={styles.body}>
        <TextInput
          placeholder="Masukkan Nama Lengkap"
          style={styles.txtInput}
          value={nama}
          onChangeText={text => setNama(text)}
        />
        <TextInput
          placeholder="Masukkan Email"
          style={styles.txtInput}
          value={email}
          onChangeText={text => setEmail(text)}
          keyboardType='email-address'
        />
        <TextInput
          placeholder="Masukkan Telepon"
          style={styles.txtInput}
          value={telepon}
          onChangeText={text => setTelepon(text)}
          keyboardType='number-pad'
        />
        <TextInput
          placeholder="Masukkan Alamat"
          textAlignVertical="top"
          style={[styles.txtInput, {height: 120}]}
          value={alamat}
          multiline={true}
          onChangeText={text => setAlamat(text)}
        />
        <TouchableOpacity
          style={styles.btn}
          onPress={() => {
            if (route.params) {
              updateData();
            } else {
              storeData();
            }
          }}>
          <Text style={styles.btntxt}>{route.params ? 'Ubah' : 'Tambah'}</Text>
        </TouchableOpacity>
        {route.params && (
          <TouchableOpacity
            style={[styles.btn, {backgroundColor: '#dd2c00'}]}
            onPress={deleteData}>
            <Text style={{fontSize: 14, color: '#fff', fontWeight: '600'}}>
              Hapus
            </Text>
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

export default AddData;

const styles = StyleSheet.create({
  btn: {
    marginTop: 20,
    width: '100%',
    backgroundColor: '#43a047',
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 15,
  },
  btntxt: {
    fontSize: 14,
    color: 'white',
    fontWeight: '600',
  },
  txtInput: {
    width: '100%',
    borderRadius: 6,
    borderColor: '#dedede',
    borderWidth: 1,
    paddingHorizontal: 10,
    marginTop: 20,
  },
  body: {
    flex: 1,
    padding: 15,
  },
  titletxt: {
    color: '#fff',
    fontSize: 16,
    fontWeight: '600',
  },
  titlebox: {
    width: '70%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
  },
  header: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#3F51b5',
    paddingVertical: 15,
  },
  backbtn: {
    width: '15%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
