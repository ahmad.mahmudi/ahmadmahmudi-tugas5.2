import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';

const Login = ({navigation, route}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const url = 'https://staging.api.autotrust.id/api/v1/';
  const dispatch = useDispatch();
  const {isLoggedIn} = useSelector(state => state.auth);

  useEffect(()=> {
    if (isLoggedIn) {navigation.replace('Home')}
  },[])

  const onLogin = async () => {
    const data = {
      email,
      password,
    };
    try {
      const response = await fetch(`${url}user/login`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      });
      const result = await response.json();
      console.log('Success:', result);
      if (result.code === 200) {
        dispatch({type: "LOGIN_SUCCESS", data})
        navigation.replace('Home');
        ToastAndroid.showWithGravity(
          'Login Sukses',
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
        );
      }
    } catch (error) {
      console.log('ERROR:', error);
      ToastAndroid.showWithGravity(
        'Login Gagal, email atau password salah',
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
      );
    }
  };
  return (
    <View style={styles.container}>
      <Text style={styles.logintxt}>Login</Text>
      <View style={styles.inputbox}>
        <Text style={styles.inputtxt}>Masukkan Email</Text>
        <TextInput
          style={styles.input}
          placeholder="Masukkan Email"
          keyboardType="email-address"
          onChangeText={value => setEmail(value)}
        />
      </View>
      <View style={styles.inputbox}>
        <Text style={styles.inputtxt}>Masukkan Password</Text>
        <TextInput
          style={styles.input}
          placeholder="Masukkan Password"
          secureTextEntry={true}
          onChangeText={value => setPassword(value)}
        />
      </View>
      <TouchableOpacity style={styles.btn} onPress={onLogin}>
        <Text style={styles.btntxt}>Log In</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logintxt: {
    fontSize: 20,
    color: '#000',
    fontWeight: 'bold',
  },
  inputbox: {
    width: '80%',
    marginTop: 15,
  },
  inputtxt: {},
  input: {
    width: '100%',
    borderRadius: 6,
    borderColor: '#dedede',
    borderWidth: 1,
    paddingHorizontal: 10,
    marginTop: 15,
  },
  btn: {
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 12,
    backgroundColor: 'green',
    borderRadius: 6,
    marginTop: 20,
  },
  btntxt: {
    color: '#ffffff',
    fontSize: 14,
    fontWeight: '500',
  },
});
